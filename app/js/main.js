console.log('main');
function mobileMenu(btn){
    btn.closest('.header').classList.toggle('header--open')
}
function changeType(btn){
    btn.closest('.header').classList.toggle('header--employee')
    btn.closest('.header').classList.toggle('header--employer')
}

function changeTab(btn){
    if (!btn.classList.contains('tab__item--active')){
        console.log('click');
        let btnList = btn.closest('.tab').querySelectorAll('[data-toogle]');
        btnList.forEach(control => { 
            control.classList.remove('tab__item--active');
        });
        btn.classList.add('tab__item--active');
        let tabList = btn.closest('.tab').querySelectorAll('[data-tab]');
        tabList.forEach(item => { 
            item.classList.remove('tab__content--active');
            console.log(item);
        });
        btn.closest('.tab').querySelector(`[data-tab="${btn.getAttribute('data-toogle')}"]`).classList.add('tab__content--active');
    }
}

function showOpenPosition(btn){
    btn.closest('.company-open').classList.toggle('company-open--show');
}

if (document.querySelector('.range__item')){
    for ( let range of document.querySelectorAll('.range__item')){
        noUiSlider.create(range, {
            start: [200000, 500000],
            connect: true,
            range: {
                'min': 20000,
                'max': 600000
            }
        });
        range.noUiSlider.on(
            'update', 
            function(){
                range.closest('.range').querySelector('[name="minIncome"]').value = range.noUiSlider.get()[0]
                range.closest('.range').querySelector('[name="maxIncome"]').value = range.noUiSlider.get()[1]
            }
        );

    }
}
if (document.querySelector('.range__item--number')){
    for ( let range of document.querySelectorAll('.range__item--number')){
        noUiSlider.create(range, {
            start: [80, 300],
            connect: true,
            range: {
                'min': 20,
                'max': 600
            }
        });
        range.noUiSlider.on(
            'update', 
            function(){
                range.closest('.range').querySelector('[name="minIncome"]').value = range.noUiSlider.get()[0]
                range.closest('.range').querySelector('[name="maxIncome"]').value = range.noUiSlider.get()[1]
            }
        );

    }
}

function showFilter(elem){
    elem.classList.toggle('select--open');
    document.querySelector('.job-filter').classList.toggle('job-filter--mobile-open');
}

function edit(elem){
    elem.closest('.cv-step').querySelector('input').disabled = false;
    elem.style.display = 'none';
}

function showLocation(elem){
    document.querySelector('.header').classList.toggle('header--location');
}